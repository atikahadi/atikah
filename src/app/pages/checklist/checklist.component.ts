import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {

  setup= ['Onboarding Session with HR, OSHE, etc.','Meet your supervisor','Collect access card and laptop',
'Setup email and password','Request for software installation', 'Join invitation for Slack, Jira and Microsoft Teams',
'Collect CML (Company Mobile Line)', 'Receive CMP voucher through email attachment from HR', 'Fill up details for Friday Stand-Up'];

  software=['Git (optional Sourcetree/Github Desktop)','git-flow','Bitbucket.org','Microsoft Visual Studio Code - Recommended plugins: Browser Preview, Prettier, GitLens'];
  constructor() { }

  ngOnInit(): void {
  }

}
