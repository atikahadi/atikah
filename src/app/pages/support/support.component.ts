import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {

  sites = [
    {name:"New Hire Onboarding Site", url:"https://sites.google.com/view/new-hire-onboarding-site/home"},
    {name:"Celcom Blue Net", url:"https://celcombluenet.celcom.com.my"},
    {name:"Sapphire", url:"https://performancemanager10.successfactors.com/sf/start?_s.crb=C6e%252fYKATicnWiveLP7znmCgTZKY%253d#/login"},
    {name:"IT Service Request Management", url:"https://itsm.celcom.com.my/arsys/shared/login.jsp?/arsys/"},
    {name:"Celcom Design JIRA", url:"https://celcomaxiata.atlassian.net/secure/RapidBoard.jspa?rapidView=182&projectKey=DSG&selectedIssue=DSG-653"}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
