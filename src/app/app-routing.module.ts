import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ChecklistComponent } from './pages/checklist/checklist.component';
import { AboutComponent } from './pages/about/about.component';
import { SupportComponent } from './pages/support/support.component';

const routes: Routes = [ 
  { path: '', redirectTo: '/home', pathMatch:'full' },
  { path:'home', component: HomeComponent },
  { path:'checklist', component: ChecklistComponent },
  { path:'about', component: AboutComponent },
  { path:'support', component: SupportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingcomponents =[HomeComponent, ChecklistComponent, AboutComponent,SupportComponent]
